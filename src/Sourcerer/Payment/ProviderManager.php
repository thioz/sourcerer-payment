<?php
namespace Sourcerer\Payment;

class ProviderManager 
{
	protected $app;
	protected $_factory;
	protected $_providers = array();
	
	public function __construct($app, $factory)
	{
		$this->app = $app;
		$this->_factory = $factory;
	}
	
	function get($name = null)
	{
		
		$name = $name !== null ? $name : $this->getDefaultProvider();
		if ( ! isset($this->_providers[$name]))
		{
			$provider = $this->makeProvider($name);

			$this->_providers[$name] = $provider;
		}

		return $this->_providers[$name];		
		
	}
	
	function makeProvider($name)
	{
		$config = $this->getConfig($name);
		return $this->_factory->make($config, $name);
	}
	
	function getConfig($name)
	{
		$config = $this->app['config']['payment.providers'];
		return isset($config[$name]) ? $config[$name] : array();
	}
	
	function getDefaultProvider()
	{
		return $this->app['config']['payment.default'];
	}
	
}
