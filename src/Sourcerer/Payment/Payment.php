<?php
namespace Sourcerer\Payment;

class Payment
{
	protected $options;
	protected $data;
	
	protected $id;
	protected $status;
	protected $paymentUrl;
	protected $description;
	protected $mode;
	
	const STATUS_OPEN = 'open';
	const STATUS_CANCELLED = 'cancelled';
	const STATUS_PENDING = 'pending';
	const STATUS_EXPIRED = 'expired';
	const STATUS_PAID = 'paid';
	const STATUS_PAIDOUT = 'paidout';
	const STATUS_REFUNDED = 'refunded';
	
	function __construct($data = null)
	{
		$this->data = $data;
	}
	
	function getPaymentUrl()
	{
		return $this->paymentUrl;
	}
	
	function setOptions($options)
	{
		$this->options = $options;
	}
	
	function getStatus()
	{
		return $this->status;
	}
	
	function getParsedStatus()
	{
		return false;
	}
	
	function getMode()
	{
		return $this->mode;
	}
	
	function getDescription()
	{
		return $this->description;
	}
	
	function getId()
	{
		return $this->id;
	}
	
	function hasOption($option)
	{
		return array_get($this->options, $option) !== null ? true : false;
	}
	
	function getOption($option)
	{
		return array_get($this->options, $option);
	}
	
}
