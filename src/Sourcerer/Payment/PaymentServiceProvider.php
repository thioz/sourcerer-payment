<?php
namespace Sourcerer\Payment;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app['payment.provider.factory'] = $this->app->share(function($app)
		{
			return new ProviderFactory($app);
		});
		
		$this->app['payment.provider'] = $this->app->share(function($app)
		{
			return new ProviderManager($app, $app['payment.provider.factory']);
		});
		
		/**
		 * register route for callback
		 */
			
		\Route::post( '/payment/callback/{name}', array('uses' => 'Sourcerer\\Payment\\Controller\\CallbackController@handle'));
		\Route::get( '/payment/callback/{name}', array('uses' => 'Sourcerer\\Payment\\Controller\\CallbackController@handle'));
		
		
	}

}
