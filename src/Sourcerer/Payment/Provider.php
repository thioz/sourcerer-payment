<?php
namespace Sourcerer\Payment;

abstract class Provider
{
	protected $options;
	
	function __construct($options)
	{
		$this->options = $options;
	}
	
	abstract function getPayment($id);
	
	abstract function getPaymentMethods();
	
	abstract function getPaymentMethod($id);
	
	abstract function getPaymentFromRequest($input = false);

	abstract function createPayment($method, $amount, $description, $data);
	
	abstract function createPaymentMethod($options);
	
}
