<?php
namespace Sourcerer\Payment;

use Illuminate\Foundation\Application;

class ProviderFactory
{
	/**
	 *
	 * @var Application
	 */
	protected $app;
	public function __construct($app)
	{
		$this->app = $app;
	}
	
	function make($config, $name)
	{
		$config += array('name' => $name);
		
		return $this->createProvider($config['driver'], $config);
		
	}
	
	function createProvider($driver, $config)
	{
		if($this->app->bound('payment.provider.'.$driver))
		{
			return $this->app->make('payment.provider.'.$driver);
		}
		
		switch($driver)
		{
			case 'mollie':
				return new Provider\Mollie($config);
		}
		

	}
}
