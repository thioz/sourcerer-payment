<?php
namespace Sourcerer\Payment;

class Method
{
	protected $id;
	protected $description;
	
	protected $minAmount;
	protected $maxAmount;
	
	protected $options = array();
	
	function __construct($options = array())
	{
		$this->options = (array) $options;
	}
	
	function supportsAmount($amount)
	{
		if(!$this->minAmount && !$this->maxAmount)
		{
			return true;
		}
		
		if($this->minAmount)
		{
			if($amount < $this->minAmount)
			{
				return false;
			}
		}
		
		if($this->maxAmount)
		{
			if($amount > $this->maxAmount)
			{
				return false;
			}
		}
		
		return true;
	}
	
	function getId()
	{
		return $this->id;
	}
	
	function getDescription()
	{
		return $this->description;
	}
	
	function id()
	{
		return $this->getId();
	}
	
	function description()
	{
		return $this->description;
	}

	
	function setId($id)
	{
		$this->id = $id;
		return $this;
	}
	
	
	function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}
	
	
	function setAmountLimits($min = null, $max = null)
	{
		$this->minAmount = $min;
		$this->maxAmount = $max;
		return $this;
	}
	
	function hasOption($option)
	{
		return array_get($this->options, $option) !== null ? true : false;
	}
	
	function getOption($option)
	{
		return array_get($this->options, $option);
	}
	
	function option($option)
	{
		return $this->getOption($option);
	}
	
	function getImage()
	{
		return false;
	}
	
	function setOption($key, $value)
	{
		array_set($this->options, $key, $value);
		return $this;
	}
	
	function __get($name)
	{
		return $this->option($name);
	}
	
	function __set($name, $value)
	{
		return $this->setOption($name, $value);
	}
	
	
}
