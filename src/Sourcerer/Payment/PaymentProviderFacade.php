<?php
namespace Sourcerer\Payment;

use Illuminate\Support\Facades\Facade;

class PaymentProviderFacade extends Facade
{
	public static function getFacadeAccessor()
	{
		return 'payment.provider';
	}
}
