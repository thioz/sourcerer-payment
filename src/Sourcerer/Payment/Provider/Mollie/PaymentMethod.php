<?php
namespace Sourcerer\Payment\Provider\Mollie;

use Sourcerer\Payment\Method;

class PaymentMethod extends Method
{
	
	function getImage()
	{
		if(isset($this->options['image']))
		{
			return $this->options['image']['normal'];
		}
		return false;
	}
}
