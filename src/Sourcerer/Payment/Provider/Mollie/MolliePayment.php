<?php
namespace Sourcerer\Payment\Provider\Mollie;

use Sourcerer\Payment\Payment;

class MolliePayment extends Payment
{
	function __construct($data = null)
	{
		parent::__construct($data);
		
		$this->id = $data->id;
		$this->status = $data->status;
		$this->paymentUrl = $data->getPaymentUrl();
		$this->description = $data->description;
		$this->mode = $data->mode;
		
		$this->parseOptions();
		
	}
	
	function parseOptions()
	{
		$options = array();
		
		$keys = array(
			'mode',
			'createdDatetime',
			'paidDatetime',
			'cancelledDatetime',
			'cancelledDatetime',
			'expiredDatetime',
			'expiryPeriod',
			'amount',
			'amount',
			'amountRefunded',
			'method',
			'metadata',
			'locale',
			'details',
		);
		
		foreach($keys as $name)
		{
			try{
				if(isset($this->data->$name))
				{
					$val = isset($this->data->$name);
					if(is_object($val))
					{
						$val = (array) $val;
					}
					$options[$name] = $val;
				}
			} catch (Exception $ex) {

			}
		}
		
		$this->setOptions($options);
		
	}
	
	
	
	function getParsedStatus()
	{
		return $this->getStatus();
		
	}
	
}
