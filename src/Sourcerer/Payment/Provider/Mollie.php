<?php
namespace Sourcerer\Payment\Provider;

use Exception;
use Mollie_API_Client;
use Sourcerer\Payment\Method;
use Sourcerer\Payment\Provider;
use Sourcerer\Payment\Provider\Mollie\MolliePayment;

class Mollie extends Provider
{
	/**
	 *
	 * @var Mollie_API_Client
	 */
	protected $client;
	
	public function __construct($options)
	{
		parent::__construct($options);
	}
	
	function getPaymentMethods()
	{
		try
		{
			$methods = array();
			
			$results = $this->getClient()->methods->all();
			if($results)
			{
				foreach($results as $item)
				{
					$data = json_decode(json_encode($item), true);
					$method = $this->createPaymentMethod($data);
					$method->setId($item->id);
					$method->setDescription($item->description);
					$method->setAmountLimits($item->amount->minimum, $item->amount->maximum);
					$methods[$item->id] = $method;
				}
			}
			
			return $methods;
			
		} catch (Exception $e) {
			
		}
		return false;
	}
	
	function getPayment($id)
	{
		try{
			$data = $this->getClient()->payments->get($id);
			
			return new MolliePayment($data);
			
			echo '<pre>';
			print_r($data);
			echo '</pre>';
			
		} catch (Exception $ex) {
			
		}
		
	}
	
	function createPayment($method, $amount, $description, $data)
	{
		$paymentData = $this->createPaymentData($method, $amount, $description, $data);
		if($paymentData)
		{
			try{
				
				$payment = $this->getClient()->payments->create($paymentData);
				return new MolliePayment($payment);
				
				
			} catch (Exception $e) {
			}
			
			
		}
		return false;
	}
	
	function createPaymentData($method, $amount, $description, $data)
	{
		if(!isset($data['redirectUrl']))
		{
			return false;
		}
		
		$paymentData = array(
			'amount' => $amount,
			'description' => $description,
			'method' => $method->id(),
			'redirectUrl' => $data['redirectUrl'],
		);
		
		if(isset($data['meta']) && is_array($data['meta']))
		{
			$paymentData['metadata'] = $data['meta'];
		}
		
		if(isset($data['webhookUrl']))
		{
			//$paymentData['webhookUrl'] = $data['webhookUrl'];
		}
		else
		{
			//$paymentData['webhookUrl'] = action('Sourcerer\\Payment\\Controller\\CallbackController@handle', $method->id());
		}

		if(isset($data['locale']))
		{
			$paymentData['locale'] = $data['locale'];
		}
		
		return $paymentData;
		
		
		
	}
	
	function getPaymentMethod($id)
	{
		$methods = $this->getPaymentMethods();
		if($methods)
		{
			return isset($methods[$id]) ? $methods[$id] : false;
		}
		return false;
	}
	
	/**
	 * 
	 * @return Mollie_API_Client
	 */
	
	function getClient()
	{
		if(!$this->client)
		{
			$this->client = new Mollie_API_Client();
			$this->client->setApiKey($this->options['api_key']);
			
		}
		
		return $this->client;
	}

	public function getPaymentFromRequest($input = false)
	{
		$input = $input !== false ? $input : \Input::all();
		
		$paymentId = isset($input['id']) ? $input['id'] : false;
		
		if(!$paymentId)
		{
			return false;
		}
		
		return $this->getPayment($paymentId);
	}

	public function createPaymentMethod($options)
	{
		return new Mollie\PaymentMethod($options);
	}

}
